$( document ).ready(function() {

	$('.hamburger').click(function(){
		$(this).toggleClass('is-active');
		$(this).siblings('.top__nav__list').toggleClass('opened');
	});

	$('.top__nav__item').click(function(){
		var winWidth = $(window).width();
		if (winWidth > 319 && winWidth < 1199) {
			if ($(this).hasClass('subnav')) {
				$(this).toggleClass('opened');
				$(this).children('.top__nav__subnav').slideToggle();
			}
		}
	});

	var mainSlider = new Swiper('.swiper-container', {
		slidesPerView: 1,
		pagination: {
      el: '.main__slider__pagination',
    },
    navigation: {
      nextEl: '.main__slider__btn.next',
      prevEl: '.main__slider__btn.prev',
    },
  });

  if ($('.main__slider__item').length < 2) {
    $('.main__slider__btn').hide();
    $('.main__slider__pagination').hide();
  }

  $(window).scroll(function(){
  	if ($(this).scrollTop() > 75) {
  		$('.nav__wrap').addClass('fixed');
  	} else {
  		$('.nav__wrap').removeClass('fixed');
  	}
  });

  if ($('.section__for__tags__item').length) {
    var sectionTags = document.querySelectorAll('.section__for__tags__item');
    var arrSectionTags = Array.prototype.slice.call(sectionTags);
    var arrSectionTagsLast = document.querySelector('.section__for__tags__item.more');
    arrSectionTagsLast.style.display = 'none';
    arrSectionTagsLast.addEventListener('click', tagToggle);

    if (arrSectionTags.length > 6) {
      arrSectionTagsLast.style.display = 'block';
      var arrSectionTagsHide = arrSectionTags.slice(6, -1);
      for (var i = 0; i < arrSectionTagsHide.length; i++) {
        arrSectionTagsHide[i].style.display = 'none';
        arrSectionTagsHide[i].classList.add('hide');
      }
    }

    function tagToggle (e) {
      e.preventDefault();
      if (this.innerText == 'Ещё') {
        this.innerText = 'Скрыть';
      } else {
        this.innerText = 'Ещё';
      }
      for (var i = 0; i < arrSectionTagsHide.length; i++) {
        arrSectionTagsHide[i].style.display = arrSectionTagsHide[i].style.display === 'none' ? '' : 'none';
      }
      setTimeout(function(){
        for (var i = 0; i < arrSectionTagsHide.length; i++) {
          arrSectionTagsHide[i].classList.toggle('show');
        }
      }, 25);
    }
  }
  

  


});